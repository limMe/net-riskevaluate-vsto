﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Excel;

namespace RiskEvaluate
{
    public partial class ThisAddIn
    {
        public Microsoft.Office.Tools.CustomTaskPane helpTaskPane;
        public Microsoft.Office.Tools.CustomTaskPane questionPanel;
        public Microsoft.Office.Tools.CustomTaskPane answerPanel;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            helpTaskPane = Globals.ThisAddIn.CustomTaskPanes.Add(new EvaluaterControl(), "风险分析");
            //helpTaskPane.Visible = true;
            helpTaskPane.VisibleChanged += new EventHandler(helpTaskPane_VisibleChanged);

            questionPanel = Globals.ThisAddIn.CustomTaskPanes.Add(new QuestionControl(),"编辑题目");
            questionPanel.VisibleChanged += new EventHandler(questionPanle_VisibleChanged);

            answerPanel = Globals.ThisAddIn.CustomTaskPanes.Add(new AnswerControl(), "填写问卷");
            answerPanel.VisibleChanged += new EventHandler(answer_VisibleChanged);

            

        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        private void helpTaskPane_VisibleChanged(object sender, EventArgs e)
        {
            // 获得Help Ribbon 对象
            //EvaluateRibbon helpRibbon = Globals.Ribbons.GetRibbon<EvaluateRibbon>();
            // 同步Help Ribbon下的"帮助"按钮的状态
            //helpRibbon.settingBtn.Checked = Globals.ThisAddIn.helpTaskPane.Visible;
        }

        private void questionPanle_VisibleChanged(object sender, EventArgs e)
        {
            EvaluateRibbon ribbon = Globals.Ribbons.GetRibbon<EvaluateRibbon>();
            ribbon.questionBtn.Checked = Globals.ThisAddIn.questionPanel.Visible;
        }

        private void answer_VisibleChanged(object sender,EventArgs e)
        {
            EvaluateRibbon ribbon = Globals.Ribbons.GetRibbon<EvaluateRibbon>();
            ribbon.answerBtn.Checked = Globals.ThisAddIn.questionPanel.Visible;
        }


        #region VSTO 生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
