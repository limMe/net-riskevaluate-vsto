﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;

namespace RiskEvaluate
{
    public partial class AnswerControl : UserControl
    {
        public AnswerControl()
        {
            InitializeComponent();
        }

        private List<QuestionModel> questions;
        private int questionsArg = 0;
        private int detailQuestionArg = 0;

        private bool readQuestions()
        {
            questions = new List<QuestionModel>();
             Microsoft.Office.Interop.Excel.Workbook nativeWorkbook =
       Globals.ThisAddIn.Application.ActiveWorkbook;
             if (nativeWorkbook != null)
             {
                 Microsoft.Office.Tools.Excel.Workbook vstoWorkbook =
                     Globals.Factory.GetVstoObject(nativeWorkbook);
                 Excel.Worksheet worksheet = (Excel.Worksheet)vstoWorkbook.Worksheets["题目设置"];

                 for (int row = 2; row < worksheet.UsedRange.Rows.Count + 1; row++)
                 {
                     Excel.Range rng1 = (Excel.Range)worksheet.Cells[row, 1];
                     //MessageBox.Show(rng1.Value2);
                     questions.Add(Newtonsoft.Json.JsonConvert.DeserializeObject<QuestionModel>(rng1.Value2));
                 }
                 return true;
             }
             else
                 return false;
        }
        private void AnswerControl_Load(object sender, EventArgs e)
        {
            
        }

        private void beginBtn_Click(object sender, EventArgs e)
        {
            try
            {
                readQuestions();
                loadNextQuestion();
                beginBtn.Visible = false;
                nextBtn.Visible = true;
                questionDescribe.Visible = true;
            }
            catch(Exception ex)
            {
                MessageBox.Show("由于发生以下错误，未能成功读取题目设置。请确保这是按照标准正确生成的文件。\n" + ex.Message);
            }
        }

        /// <summary>
        /// 不会增加目前的questionArg, detailArg, 不负责分析
        /// </summary>
        private void loadNextQuestion()
        {
            hideAllSelections();

            questionDescribe.Text = questions[questionsArg].detailQuestions[detailQuestionArg].description;

            int max_selections = questions[questionsArg].detailQuestions[detailQuestionArg].selections.Count;
            int i = 0;
            if(i<max_selections)
            {
                answer1.Visible = true;
                answer1.Text = questions[questionsArg].detailQuestions[detailQuestionArg].selections[i].description;
                i++;
            }

            if (i < max_selections)
            {
                answer2.Visible = true;
                answer2.Text = questions[questionsArg].detailQuestions[detailQuestionArg].selections[i].description;
                i++;
            }
            if (i < max_selections)
            {
                answer3.Visible = true;
                answer3.Text = questions[questionsArg].detailQuestions[detailQuestionArg].selections[i].description;
                i++;
            }
            if (i < max_selections)
            {
                answer4.Visible = true;
                answer4.Text = questions[questionsArg].detailQuestions[detailQuestionArg].selections[i].description;
                i++;
            }
            if (i < max_selections)
            {
                answer5.Visible = true;
                answer5.Text = questions[questionsArg].detailQuestions[detailQuestionArg].selections[i].description;
                i++;
            }
            if (i < max_selections)
            {
                answer6.Visible = true;
                answer6.Text = questions[questionsArg].detailQuestions[detailQuestionArg].selections[i].description;
                i++;
            }
        }

        private void hideAllSelections()
        {
            answer1.Visible = false;
            answer2.Visible = false;
            answer3.Visible = false;
            answer4.Visible = false;
            answer5.Visible = false;
            answer6.Visible = false;

            answer1.BackColor = Color.White;
            answer2.BackColor = Color.White;
            answer3.BackColor = Color.White;
            answer4.BackColor = Color.White;
            answer5.BackColor = Color.White;
            answer6.BackColor = Color.White;
        }

        private void nextBtn_Click(object sender, EventArgs e)
        {
            if (answer1.BackColor == Color.AliceBlue
                || answer2.BackColor == Color.AliceBlue
                || answer3.BackColor == Color.AliceBlue
                || answer4.BackColor == Color.AliceBlue
                || answer5.BackColor == Color.AliceBlue
                || answer6.BackColor == Color.AliceBlue)
            {
                questions[questionsArg].detailQuestions[detailQuestionArg].selected.Remove(-1);
                if (questionsArg == (questions.Count - 1)
                    && detailQuestionArg == (questions[questionsArg].detailQuestions.Count - 1))
                {
                    hideAllSelections();
                    analysist(questions[questionsArg]);
                    MessageBox.Show("您已完成问卷填写");
                    beginBtn.Visible = true;
                    questionDescribe.Visible = false;
                    nextBtn.Visible = false;
                    questionsArg = 0;
                    detailQuestionArg = 0;
                    return;
                }
                if (detailQuestionArg == questions[questionsArg].detailQuestions.Count - 1)
                {
                    analysist(questions[questionsArg]);
                    questionsArg++;
                    detailQuestionArg = 0;
                }
                else
                {
                    detailQuestionArg++;
                }
                loadNextQuestion();
            }
            else
            {
                MessageBox.Show("你还没有回答当前问题");
            }
        }

        private void answer1_KeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if (answer1.BackColor != Color.AliceBlue)
                {
                    answer1.BackColor = Color.AliceBlue;
                    questions[questionsArg].detailQuestions[detailQuestionArg].selected.Add(0);
                    if (questions[questionsArg].detailQuestions[detailQuestionArg].isMulti == false)
                    {
                        nextBtn_Click(this, null);
                    }
                }
                else
                {
                    answer1.BackColor = Color.White;
                    questions[questionsArg].detailQuestions[detailQuestionArg].selected.Remove(0);
                }
            }
        }

        private void answer2_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (answer2.BackColor != Color.AliceBlue)
                {
                    answer2.BackColor = Color.AliceBlue;
                    questions[questionsArg].detailQuestions[detailQuestionArg].selected.Add(1);
                    if (questions[questionsArg].detailQuestions[detailQuestionArg].isMulti == false)
                    {
                        nextBtn_Click(this, null);
                    }
                }
                else
                {
                    answer2.BackColor = Color.White;
                    questions[questionsArg].detailQuestions[detailQuestionArg].selected.Remove(1);
                }
            }
        }

        private void answer3_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (answer3.BackColor != Color.AliceBlue)
                {
                    answer3.BackColor = Color.AliceBlue;
                    questions[questionsArg].detailQuestions[detailQuestionArg].selected.Add(2);
                    if (questions[questionsArg].detailQuestions[detailQuestionArg].isMulti == false)
                    {
                        nextBtn_Click(this, null);
                    }
                }
                else
                {
                    answer3.BackColor = Color.White;
                    questions[questionsArg].detailQuestions[detailQuestionArg].selected.Remove(2);
                }
            }
        }

        private void answer4_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (answer4.BackColor != Color.AliceBlue)
                {
                    answer4.BackColor = Color.AliceBlue;
                    questions[questionsArg].detailQuestions[detailQuestionArg].selected.Add(3);
                    if (questions[questionsArg].detailQuestions[detailQuestionArg].isMulti == false)
                    {
                        nextBtn_Click(this, null);
                    }
                }
                else
                {
                    answer4.BackColor = Color.White;
                    questions[questionsArg].detailQuestions[detailQuestionArg].selected.Remove(3);
                }
            }
        }

        private void answer5_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (answer5.BackColor != Color.AliceBlue)
                {
                    answer5.BackColor = Color.AliceBlue;
                    questions[questionsArg].detailQuestions[detailQuestionArg].selected.Add(4);
                    if (questions[questionsArg].detailQuestions[detailQuestionArg].isMulti == false)
                    {
                        nextBtn_Click(this, null);
                    }
                }
                else
                {
                    answer5.BackColor = Color.White;
                    questions[questionsArg].detailQuestions[detailQuestionArg].selected.Remove(4);
                }
            }
        }

        private void answer6_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (answer6.BackColor != Color.AliceBlue)
                {
                    answer6.BackColor = Color.AliceBlue;
                    questions[questionsArg].detailQuestions[detailQuestionArg].selected.Add(5);
                    if (questions[questionsArg].detailQuestions[detailQuestionArg].isMulti == false)
                    {
                        nextBtn_Click(this, null);
                    }
                }
                else
                {
                    answer6.BackColor = Color.White;
                    questions[questionsArg].detailQuestions[detailQuestionArg].selected.Remove(5);
                }
            }
        }

        private void analysist(QuestionModel questionToAnalysis)
        {
            decimal result = 0.0M;

            //************************
            //相加求和除以最大值求和
            //************************
            if(questionToAnalysis.analysisLogic == (int)AnalysisLogic.Sum)
            {
                int sumMaxWeight = 0;
                int sumChocieWeight = 0;
                for(int count=0;count < questionToAnalysis.detailQuestions.Count; count++ )
                {
                    if(questionToAnalysis.detailQuestions[count].isMulti)
                    {
                        sumMaxWeight += getMultiAllWeight(questionToAnalysis.detailQuestions[count]);
                        sumChocieWeight += getMultiChoicesWeight(questionToAnalysis.detailQuestions[count]);
                    }
                    else
                    {
                        sumMaxWeight += getMaxWeight(questionToAnalysis.detailQuestions[count]);
                        sumChocieWeight += getMultiChoicesWeight(questionToAnalysis.detailQuestions[count]);
                    }
                }
                result = (decimal)sumChocieWeight / (decimal)sumMaxWeight;
            }

            //************************
            //相乘除以最大风险
            //************************
            if(questionToAnalysis.analysisLogic == (int)AnalysisLogic.Multi)
            {
                ulong productMaxWeight = 1;
                ulong productChoiceWeight = 1;
                for(int count =0; count<questionToAnalysis.detailQuestions.Count;count++)
                {
                    productChoiceWeight = productChoiceWeight * change10to2(getMultiChoicesWeight(questionToAnalysis.detailQuestions[count]));
                    productMaxWeight = productMaxWeight * change10to2(getMaxWeight(questionToAnalysis.detailQuestions[count]));
                }
                //MessageBox.Show(productChoiceWeight.ToString() + " " + productMaxWeight.ToString());
                result = (decimal)productChoiceWeight / (decimal)productMaxWeight;
                result = (decimal)Math.Log((double)productChoiceWeight, (double)productMaxWeight);
            }

            //************************
            //根据最大风险
            //************************
            if(questionToAnalysis.analysisLogic == (int)AnalysisLogic.Maxer)
            {
                int maxChoiceWeight = 0;
                int maxMaxWeight = 0;
                for (int count = 0; count < questionToAnalysis.detailQuestions.Count; count++)
                {
                    maxChoiceWeight = getMultiChoicesWeight(questionToAnalysis.detailQuestions[count]);
                    maxMaxWeight = getMaxWeight(questionToAnalysis.detailQuestions[count]);
                }
                result = (decimal)maxChoiceWeight / (decimal)maxMaxWeight;
            }

            //************************
            //6_1
            //************************
            if(questionToAnalysis.analysisLogic == (int)(AnalysisLogic.Logic6_1))
            {
                int sumMaxWeight = 0;
                int sumChocieWeight = 0;
                for(int count=0;count < questionToAnalysis.detailQuestions.Count; count++ )
                {
                    if(questionToAnalysis.detailQuestions[count].isMulti)
                    {
                        sumMaxWeight += getMultiAllWeight(questionToAnalysis.detailQuestions[count]);
                    }
                    else
                    {
                        sumMaxWeight += getMaxWeight(questionToAnalysis.detailQuestions[count]);
                    }
                }
                if(questionToAnalysis.detailQuestions[0].selected[0]==1)
                {
                    sumChocieWeight += getMultiChoicesWeight(questionToAnalysis.detailQuestions[0]);
                }
                else
                {
                    sumChocieWeight += getMultiChoicesWeight(questionToAnalysis.detailQuestions[0]);
                    if(questionToAnalysis.detailQuestions[1].selected[0]==1)
                    {
                        sumChocieWeight += getMultiChoicesWeight(questionToAnalysis.detailQuestions[1]);
                    }
                    else
                    {
                        sumChocieWeight += getMultiChoicesWeight(questionToAnalysis.detailQuestions[1]);
                        sumChocieWeight += getMultiChoicesWeight(questionToAnalysis.detailQuestions[2]);
                    }
                }
                result = (decimal)sumChocieWeight / (decimal)sumMaxWeight;
            }

            //************************
            //6_2
            //************************
            if(questionToAnalysis.analysisLogic == (int)AnalysisLogic.Logic6_2)
            {
                int sumMaxWeight = 0;
                int sumChocieWeight = 0;
                for(int count=0;count < questionToAnalysis.detailQuestions.Count; count++ )
                {
                    if(questionToAnalysis.detailQuestions[count].isMulti)
                    {
                        sumMaxWeight += getMultiAllWeight(questionToAnalysis.detailQuestions[count]);
                    }
                    else
                    {
                        sumMaxWeight += getMaxWeight(questionToAnalysis.detailQuestions[count]);
                    }
                }
                if(questionToAnalysis.detailQuestions[0].selected[0]==1)
                {
                    sumChocieWeight += getMultiChoicesWeight(questionToAnalysis.detailQuestions[0]);
                }
                else
                {
                    sumChocieWeight += getMultiChoicesWeight(questionToAnalysis.detailQuestions[0]);
                    sumChocieWeight += getMultiChoicesWeight(questionToAnalysis.detailQuestions[1]);
                }
                result = (decimal)sumChocieWeight / (decimal)sumMaxWeight;
            }

            //************************
            //相加求和除以最大值求和,多选作为加分项处理
            //************************
            if (questionToAnalysis.analysisLogic == (int)AnalysisLogic.Sum)
            {
                int sumMaxWeight = 0;
                int sumChocieWeight = 0;
                for (int count = 0; count < questionToAnalysis.detailQuestions.Count; count++)
                {
                    if (questionToAnalysis.detailQuestions[count].isMulti)
                    {
                        //if()
                        sumMaxWeight += getMaxWeight(questionToAnalysis.detailQuestions[count]);
                        sumChocieWeight += getMultiChoicesWeight(questionToAnalysis.detailQuestions[count]);
                        sumChocieWeight += getMaxWeight(questionToAnalysis.detailQuestions[count]); ;
                    }
                    else
                    {
                        sumMaxWeight += getMaxWeight(questionToAnalysis.detailQuestions[count]);
                        sumChocieWeight += getMultiChoicesWeight(questionToAnalysis.detailQuestions[count]);
                    }
                }
                result = (decimal)sumChocieWeight / (decimal)sumMaxWeight;
            }

            //************************
            //相乘除以最大风险,多选作为加分项
            //************************
            if (questionToAnalysis.analysisLogic == (int)AnalysisLogic.MultiMultiAsPlus)
            {
                ulong productMaxWeight = 1;
                ulong productChoiceWeight = 1;
                for (int count = 0; count < questionToAnalysis.detailQuestions.Count; count++)
                {
                    productChoiceWeight = productChoiceWeight * change10to2(getMultiChoicesWeight(questionToAnalysis.detailQuestions[count]));
                    ulong middle = 1;
                    for (int j = 0; j < questionToAnalysis.detailQuestions[count].selections.Count;j++ )
                    {
                        //MessageBox.Show("!");
                        middle = middle * change10to2(10);
                    }
                    productMaxWeight = productMaxWeight * middle;
                }
                //MessageBox.Show(productChoiceWeight.ToString() + " " + productMaxWeight.ToString());
                result = (decimal)productChoiceWeight / (decimal)productMaxWeight;
                result = (decimal)Math.Log((double)productChoiceWeight, (double)productMaxWeight);
            }

            //************************
            //8_3
            //************************
            if (questionToAnalysis.analysisLogic == (int)AnalysisLogic.Logic8_3)
            {
                int sumMaxWeight = 0;
                int sumChocieWeight = 0;
                for (int count = 0; count < questionToAnalysis.detailQuestions.Count; count++)
                {
                    if (questionToAnalysis.detailQuestions[count].isMulti)
                    {
                        sumMaxWeight += getMultiAllWeight(questionToAnalysis.detailQuestions[count]);
                    }
                    else
                    {
                        sumMaxWeight += getMaxWeight(questionToAnalysis.detailQuestions[count]);
                    }
                }
                if (questionToAnalysis.detailQuestions[0].selected[0] == 1)
                {
                    sumChocieWeight += getMultiChoicesWeight(questionToAnalysis.detailQuestions[0]);
                }
                else
                {
                    sumChocieWeight += getMultiChoicesWeight(questionToAnalysis.detailQuestions[0]);
                    sumChocieWeight += getMultiChoicesWeight(questionToAnalysis.detailQuestions[1]);
                }
                result = (decimal)sumChocieWeight / (decimal)sumMaxWeight;
            }

            //************************
            //1_4
            //************************
            if (questionToAnalysis.analysisLogic == (int)AnalysisLogic.Logic1_4)
            {
                ulong productMaxWeight = 1;
                ulong productChoiceWeight = 1;
                for (int count = 0; count < questionToAnalysis.detailQuestions.Count; count++)
                {
                    productChoiceWeight = productChoiceWeight * change10to2(getMultiChoicesWeight(questionToAnalysis.detailQuestions[count]));
                    productMaxWeight = productMaxWeight * change10to2(getMaxWeight(questionToAnalysis.detailQuestions[count]));
                }
                if (questionToAnalysis.detailQuestions[0].selected[0]==2)
                {
                    productChoiceWeight = productChoiceWeight 
                        * change10to2(getMultiChoicesWeight(questionToAnalysis.detailQuestions[0]))
                        * change10to2(getMultiChoicesWeight(questionToAnalysis.detailQuestions[4]));
                }
                //MessageBox.Show(productChoiceWeight.ToString() + " " + productMaxWeight.ToString());
                result = (decimal)productChoiceWeight / (decimal)productMaxWeight;
                result = (decimal)Math.Log((double)productChoiceWeight, (double)productMaxWeight);
            }

            

            //MessageBox.Show(result.ToString());
            insertNewResult(randomize(result), questionToAnalysis.bandldId);
        }


        private decimal randomize(decimal num)
        {
            int randomRange = (int)Math.Floor((num * 10000.0M) / 5.0M);
            var random = new Random();
            int randomNum = random.Next(randomRange) - randomRange / 2;
            //MessageBox.Show(randomNum.ToString());

            num = num + (decimal)randomNum / 10000.0M;
            if (num > 1.0M)
                return 1.0M;
            if (num < 0.0M)
                return 0.0M;
            return num;

        }

        private bool insertNewResult(decimal num, int bandleId)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Workbook nativeWorkbook =
                        Globals.ThisAddIn.Application.ActiveWorkbook;
                if (nativeWorkbook != null)
                {
                    Microsoft.Office.Tools.Excel.Workbook vstoWorkbook =
                        Globals.Factory.GetVstoObject(nativeWorkbook);
                    Excel.Worksheet worksheet = (Excel.Worksheet)vstoWorkbook.Worksheets["评估结果"];
                    Excel.Range rg = (Excel.Range)worksheet.Cells[bandleId + 1, 8];

                    rg.Value2 = num;
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        private ulong change10to2(int num10)
        {
            ulong result = 1;
            while((num10 = num10/10)>=1)
            {
                result = result * 2;
            }
            return result;
        }

        private int getMaxWeight(DetailQuestion detail)
        {
            int max = 0;
            for(int count=0; count < detail.selections.Count; count++)
            {
                if (max < detail.selections[count].weight)
                {
                    max = detail.selections[count].weight;
                }
            }
            return max;
        }

        private int getMinWeight(DetailQuestion detail)
        {
            int min = 0;
            for(int count=0;count<detail.selections.Count;count++)
            {
                if(min>detail.selections[count].weight)
                {
                    min = detail.selections[count].weight;
                }
            }
            return min;
        }

        private int getMultiChoicesWeight(DetailQuestion detail)
        {
            int sum = 0;
            foreach(int num in detail.selected)
            {
                sum += detail.selections[num].weight;
            }
            return sum;
        }

        private int getMultiAllWeight(DetailQuestion detail)
        {
            int sum = 0;
            for(int count =0;count<detail.selections.Count;count++)
            {
                sum += detail.selections[count].weight;
            }
            return sum;
        }

        private void answer6_Click(object sender, EventArgs e)
        {
            var newE = new PreviewKeyDownEventArgs(Keys.Enter);
            answer6_PreviewKeyDown(this, newE);
        }

        private void answer5_Click(object sender, EventArgs e)
        {
            var newE = new PreviewKeyDownEventArgs(Keys.Enter);
            answer5_PreviewKeyDown(this, newE);
        }

        private void answer4_Click(object sender, EventArgs e)
        {
            var newE = new PreviewKeyDownEventArgs(Keys.Enter);
            answer4_PreviewKeyDown(this,  newE);
        }

        private void answer3_Click(object sender, EventArgs e)
        {
            var newE = new PreviewKeyDownEventArgs(Keys.Enter);
            answer3_PreviewKeyDown(this, newE);
        }

        private void answer2_Click(object sender, EventArgs e)
        {
            var newE = new PreviewKeyDownEventArgs(Keys.Enter);
            answer2_PreviewKeyDown(this, newE);
        }

        private void answer1_Click(object sender, EventArgs e)
        {
            var newE = new PreviewKeyDownEventArgs(Keys.Enter);
            answer1_KeyDown(this, newE);
        }

        private void nextBtn_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            //nextBtn_Click(this, null);
        }
    }
}
