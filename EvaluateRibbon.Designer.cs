﻿namespace RiskEvaluate
{
    partial class EvaluateRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public EvaluateRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EvaluateRibbon));
            this.tab1 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.answerBtn = this.Factory.CreateRibbonToggleButton();
            this.questionBtn = this.Factory.CreateRibbonToggleButton();
            this.tab1.SuspendLayout();
            this.group1.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.Groups.Add(this.group1);
            this.tab1.Label = "风险分析";
            this.tab1.Name = "tab1";
            // 
            // group1
            // 
            this.group1.Items.Add(this.answerBtn);
            this.group1.Items.Add(this.questionBtn);
            this.group1.Label = "实用功能";
            this.group1.Name = "group1";
            // 
            // answerBtn
            // 
            this.answerBtn.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.answerBtn.Image = ((System.Drawing.Image)(resources.GetObject("answerBtn.Image")));
            this.answerBtn.Label = "填写问卷";
            this.answerBtn.Name = "answerBtn";
            this.answerBtn.ShowImage = true;
            this.answerBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.answerBtn_Click);
            // 
            // questionBtn
            // 
            this.questionBtn.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.questionBtn.Image = ((System.Drawing.Image)(resources.GetObject("questionBtn.Image")));
            this.questionBtn.Label = "设置题目";
            this.questionBtn.Name = "questionBtn";
            this.questionBtn.ShowImage = true;
            this.questionBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.questionBtn_Click);
            // 
            // EvaluateRibbon
            // 
            this.Name = "EvaluateRibbon";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.EvaluateRibbon_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonToggleButton questionBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonToggleButton answerBtn;
    }

    partial class ThisRibbonCollection
    {
        internal EvaluateRibbon EvaluateRibbon
        {
            get { return this.GetRibbon<EvaluateRibbon>(); }
        }
    }
}
