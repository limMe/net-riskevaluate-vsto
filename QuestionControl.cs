﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;

namespace RiskEvaluate
{
    public partial class QuestionControl : UserControl
    {
        private QuestionModel currentQuestion;
        private DetailQuestion currentDetailQuestion;
        private Selection currentSelection;

        public QuestionControl()
        {
            InitializeComponent();
            currentQuestion = new QuestionModel();
            currentDetailQuestion = new DetailQuestion();
            currentSelection = new Selection();
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void addSelectionBtn_Click(object sender, EventArgs e)
        {
            int weight;
            if(!int.TryParse(weightBox.Text,out weight))
            {
                MessageBox.Show("加权值必须为整数！");
                return;
            }
            if(selectionDescribe.Text == "")
            {
                MessageBox.Show("描述不能为空!");
                return;
            }
            currentSelection.description = selectionDescribe.Text;
            //MessageBox.Show(currentSelection.description);
            currentSelection.weight = weight;
            if(currentSelection.weight == 0)
            {
                MessageBox.Show("加权值不能为0");
                return;
            }
            if(currentDetailQuestion.selections == null)
            {
                currentDetailQuestion.selections = new List<Selection>();
            }
            currentDetailQuestion.selections.Add(currentSelection);

            numOfSelection.Text = "已添加" + currentDetailQuestion.selections.Count.ToString() + "个选项";
            currentSelection = new Selection();
            weightBox.Text = "";
            selectionDescribe.Text = "";
            selectionDescribe.Focus();
        }

        private void addDetailQuestionBtn_Click(object sender, EventArgs e)
        {
            if (weightBox.Text != "")
            {
                addSelectionBtn_Click(this, null);
            }
            if(questionDescribe.Text == "")
            {
                MessageBox.Show("题目描述不能够为空");
                return;
            }
           
            if(currentDetailQuestion.selections == null)
            {
                MessageBox.Show("题目不能够没有任何选项");
                return;
            }
            currentDetailQuestion.selected = new List<int>();
            currentDetailQuestion.selected.Add(-1);
            currentDetailQuestion.isMulti = isMultiBox.Checked;
            currentDetailQuestion.description = questionDescribe.Text;
            if(currentQuestion.detailQuestions == null)
            {
                currentQuestion.detailQuestions = new List<DetailQuestion>();
            }
            currentQuestion.detailQuestions.Add(currentDetailQuestion);
            //MessageBox.Show(currentDetailQuestion.selections[0].description);
            numOfQuestion.Text = "已添加" + currentQuestion.detailQuestions.Count.ToString() + "题";
            numOfSelection.Text = "已添加0个选项";
            currentDetailQuestion = new DetailQuestion();
            isMultiBox.Checked = false;
            questionDescribe.Text = "";
            questionDescribe.Focus();  
        }

        private void addQuestionBtn_Click(object sender, EventArgs e)
        {
            if (questionDescribe.Text != "")
            {
                addDetailQuestionBtn_Click(this, null);
            }
            int logicId, bandleId;
            if(!int.TryParse(logicBox.Text, out logicId) || !int.TryParse(bandleIdBox.Text,out bandleId))
            {
                MessageBox.Show("确保逻辑和绑定是非常小的几个正数之一");
                return;
            }
            if(currentQuestion.detailQuestions == null)
            {
                MessageBox.Show("题目中不能包含0个小题");
                return;
            }

            currentQuestion.serialNum = serialNumBox.Text;
            currentQuestion.bandldId = bandleId;
            currentQuestion.analysisLogic = logicId;
            //MessageBox.Show(currentQuestion.detailQuestions[0].selections[0].description);
            if(insertNewQuestion(Newtonsoft.Json.JsonConvert.SerializeObject(currentQuestion))==false)
            {
                MessageBox.Show("未能将问题写入文档。");
            }
            currentQuestion = new QuestionModel();
            numOfQuestion.Text = "已添加0个小题";
            numOfSelection.Text = "已添加0个选项";
            serialNumBox.Text = "";
            serialNumBox.Focus();
        }

        private void weightBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                addSelectionBtn_Click(this, null);
            }
        }

        private void questionDescribe_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                addDetailQuestionBtn_Click(this, null);
            }
        }

        private void bandleIdBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                addQuestionBtn_Click(this, null);
            }
        }

        private bool updateQuestionNums()
        {
            throw new NotImplementedException();
        }

        private bool insertNewQuestion(string str)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Workbook nativeWorkbook =
       Globals.ThisAddIn.Application.ActiveWorkbook;
                if (nativeWorkbook != null)
                {
                    Microsoft.Office.Tools.Excel.Workbook vstoWorkbook =
                        Globals.Factory.GetVstoObject(nativeWorkbook);
                    Excel.Worksheet worksheet = (Excel.Worksheet)vstoWorkbook.Worksheets["题目设置"];
                    int max_row = worksheet.UsedRange.Rows.Count;
                    Excel.Range rg = (Excel.Range)worksheet.Cells[max_row + 1, 1];

                    rg.Value2 = str;
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
