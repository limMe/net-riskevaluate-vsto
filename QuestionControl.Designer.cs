﻿namespace RiskEvaluate
{
    partial class QuestionControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.serialNumBox = new System.Windows.Forms.TextBox();
            this.logicBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.addDetailQuestionBtn = new System.Windows.Forms.Button();
            this.numOfSelection = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.addSelectionBtn = new System.Windows.Forms.Button();
            this.weightBox = new System.Windows.Forms.TextBox();
            this.selectionDescribe = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.questionDescribe = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.numOfQuestion = new System.Windows.Forms.Label();
            this.addQuestionBtn = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.bandleIdBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.isMultiBox = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "题号：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "逻辑：";
            // 
            // serialNumBox
            // 
            this.serialNumBox.Location = new System.Drawing.Point(118, 68);
            this.serialNumBox.Name = "serialNumBox";
            this.serialNumBox.Size = new System.Drawing.Size(100, 35);
            this.serialNumBox.TabIndex = 1;
            // 
            // logicBox
            // 
            this.logicBox.Location = new System.Drawing.Point(118, 127);
            this.logicBox.Name = "logicBox";
            this.logicBox.Size = new System.Drawing.Size(100, 35);
            this.logicBox.TabIndex = 2;
            this.logicBox.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label3.Location = new System.Drawing.Point(238, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(238, 24);
            this.label3.TabIndex = 4;
            this.label3.Text = "计算逻辑，大多数是0";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.isMultiBox);
            this.panel1.Controls.Add(this.addDetailQuestionBtn);
            this.panel1.Controls.Add(this.numOfSelection);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.questionDescribe);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Location = new System.Drawing.Point(55, 339);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(422, 510);
            this.panel1.TabIndex = 5;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // addDetailQuestionBtn
            // 
            this.addDetailQuestionBtn.Location = new System.Drawing.Point(264, 440);
            this.addDetailQuestionBtn.Name = "addDetailQuestionBtn";
            this.addDetailQuestionBtn.Size = new System.Drawing.Size(145, 41);
            this.addDetailQuestionBtn.TabIndex = 8;
            this.addDetailQuestionBtn.Text = "添加小题";
            this.addDetailQuestionBtn.UseVisualStyleBackColor = true;
            this.addDetailQuestionBtn.Click += new System.EventHandler(this.addDetailQuestionBtn_Click);
            // 
            // numOfSelection
            // 
            this.numOfSelection.AutoSize = true;
            this.numOfSelection.Location = new System.Drawing.Point(52, 180);
            this.numOfSelection.Name = "numOfSelection";
            this.numOfSelection.Size = new System.Drawing.Size(166, 24);
            this.numOfSelection.TabIndex = 8;
            this.numOfSelection.Text = "已添加0个选项";
            this.numOfSelection.Click += new System.EventHandler(this.label10_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.addSelectionBtn);
            this.panel2.Controls.Add(this.weightBox);
            this.panel2.Controls.Add(this.selectionDescribe);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(63, 218);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(351, 197);
            this.panel2.TabIndex = 13;
            // 
            // addSelectionBtn
            // 
            this.addSelectionBtn.Location = new System.Drawing.Point(200, 153);
            this.addSelectionBtn.Name = "addSelectionBtn";
            this.addSelectionBtn.Size = new System.Drawing.Size(145, 41);
            this.addSelectionBtn.TabIndex = 7;
            this.addSelectionBtn.Text = "添加选项";
            this.addSelectionBtn.UseVisualStyleBackColor = true;
            this.addSelectionBtn.Click += new System.EventHandler(this.addSelectionBtn_Click);
            // 
            // weightBox
            // 
            this.weightBox.Location = new System.Drawing.Point(153, 87);
            this.weightBox.Name = "weightBox";
            this.weightBox.Size = new System.Drawing.Size(192, 35);
            this.weightBox.TabIndex = 6;
            this.weightBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.weightBox_KeyDown);
            // 
            // selectionDescribe
            // 
            this.selectionDescribe.Location = new System.Drawing.Point(153, 21);
            this.selectionDescribe.Name = "selectionDescribe";
            this.selectionDescribe.Size = new System.Drawing.Size(192, 35);
            this.selectionDescribe.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(85, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 24);
            this.label7.TabIndex = 16;
            this.label7.Text = "加权：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(85, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 24);
            this.label6.TabIndex = 14;
            this.label6.Text = "描述：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(52, 128);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 24);
            this.label8.TabIndex = 7;
            this.label8.Text = "选项：";
            // 
            // questionDescribe
            // 
            this.questionDescribe.Location = new System.Drawing.Point(118, 30);
            this.questionDescribe.Name = "questionDescribe";
            this.questionDescribe.Size = new System.Drawing.Size(290, 35);
            this.questionDescribe.TabIndex = 4;
            this.questionDescribe.KeyDown += new System.Windows.Forms.KeyEventHandler(this.questionDescribe_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(50, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 24);
            this.label5.TabIndex = 7;
            this.label5.Text = "题干：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(53, 304);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 24);
            this.label4.TabIndex = 6;
            this.label4.Text = "题目：";
            // 
            // numOfQuestion
            // 
            this.numOfQuestion.AutoSize = true;
            this.numOfQuestion.Location = new System.Drawing.Point(52, 252);
            this.numOfQuestion.Name = "numOfQuestion";
            this.numOfQuestion.Size = new System.Drawing.Size(118, 24);
            this.numOfQuestion.TabIndex = 7;
            this.numOfQuestion.Text = "已添加0题";
            // 
            // addQuestionBtn
            // 
            this.addQuestionBtn.Location = new System.Drawing.Point(319, 847);
            this.addQuestionBtn.Name = "addQuestionBtn";
            this.addQuestionBtn.Size = new System.Drawing.Size(145, 41);
            this.addQuestionBtn.TabIndex = 9;
            this.addQuestionBtn.Text = "添加题目";
            this.addQuestionBtn.UseVisualStyleBackColor = true;
            this.addQuestionBtn.Click += new System.EventHandler(this.addQuestionBtn_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label9.Location = new System.Drawing.Point(239, 190);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(154, 24);
            this.label9.TabIndex = 23;
            this.label9.Text = "关联指标1-24";
            // 
            // bandleIdBox
            // 
            this.bandleIdBox.Location = new System.Drawing.Point(119, 187);
            this.bandleIdBox.Name = "bandleIdBox";
            this.bandleIdBox.Size = new System.Drawing.Size(100, 35);
            this.bandleIdBox.TabIndex = 3;
            this.bandleIdBox.Text = "0";
            this.bandleIdBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bandleIdBox_KeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(52, 190);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 24);
            this.label10.TabIndex = 21;
            this.label10.Text = "关联：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(107, 420);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 24);
            this.label11.TabIndex = 25;
            this.label11.Text = "多选：";
            // 
            // isMultiBox
            // 
            this.isMultiBox.AutoSize = true;
            this.isMultiBox.Location = new System.Drawing.Point(136, 82);
            this.isMultiBox.Name = "isMultiBox";
            this.isMultiBox.Size = new System.Drawing.Size(210, 28);
            this.isMultiBox.TabIndex = 14;
            this.isMultiBox.Text = "这是一个多选题";
            this.isMultiBox.UseVisualStyleBackColor = true;
            // 
            // QuestionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.bandleIdBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.addQuestionBtn);
            this.Controls.Add(this.numOfQuestion);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.logicBox);
            this.Controls.Add(this.serialNumBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "QuestionControl";
            this.Size = new System.Drawing.Size(539, 986);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox serialNumBox;
        private System.Windows.Forms.TextBox logicBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox questionDescribe;
        private System.Windows.Forms.Label numOfSelection;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox weightBox;
        private System.Windows.Forms.TextBox selectionDescribe;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label numOfQuestion;
        private System.Windows.Forms.Button addDetailQuestionBtn;
        private System.Windows.Forms.Button addSelectionBtn;
        private System.Windows.Forms.Button addQuestionBtn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox bandleIdBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox isMultiBox;
        private System.Windows.Forms.Label label11;
    }
}
