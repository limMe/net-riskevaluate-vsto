﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;

namespace RiskEvaluate
{
    public partial class EvaluateRibbon
    {
        private void EvaluateRibbon_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void settingBtn_Click(object sender, RibbonControlEventArgs e)
        {
            // 通过toggleHelpButton的选中状态来控制帮助任务栏的显示和隐藏
            //Globals.ThisAddIn.helpTaskPane.Visible = settingBtn.Checked;
        }

        private void questionBtn_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.questionPanel.Visible = questionBtn.Checked;
        }

        private void answerBtn_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.answerPanel.Visible = answerBtn.Checked;
        }
    }
}
