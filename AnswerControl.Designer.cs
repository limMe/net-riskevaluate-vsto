﻿namespace RiskEvaluate
{
    partial class AnswerControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.beginBtn = new System.Windows.Forms.Button();
            this.answer1 = new System.Windows.Forms.Button();
            this.answer2 = new System.Windows.Forms.Button();
            this.answer3 = new System.Windows.Forms.Button();
            this.answer4 = new System.Windows.Forms.Button();
            this.answer5 = new System.Windows.Forms.Button();
            this.answer6 = new System.Windows.Forms.Button();
            this.questionDescribe = new System.Windows.Forms.Label();
            this.nextBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // beginBtn
            // 
            this.beginBtn.Location = new System.Drawing.Point(139, 60);
            this.beginBtn.Name = "beginBtn";
            this.beginBtn.Size = new System.Drawing.Size(189, 65);
            this.beginBtn.TabIndex = 0;
            this.beginBtn.Text = "开始填写";
            this.beginBtn.UseVisualStyleBackColor = true;
            this.beginBtn.Click += new System.EventHandler(this.beginBtn_Click);
            // 
            // answer1
            // 
            this.answer1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.answer1.Location = new System.Drawing.Point(0, 274);
            this.answer1.Name = "answer1";
            this.answer1.Size = new System.Drawing.Size(463, 67);
            this.answer1.TabIndex = 1;
            this.answer1.Text = "button0";
            this.answer1.UseVisualStyleBackColor = false;
            this.answer1.Visible = false;
            this.answer1.Click += new System.EventHandler(this.answer1_Click);
            this.answer1.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.answer1_KeyDown);
            // 
            // answer2
            // 
            this.answer2.BackColor = System.Drawing.SystemColors.HighlightText;
            this.answer2.Location = new System.Drawing.Point(0, 352);
            this.answer2.Name = "answer2";
            this.answer2.Size = new System.Drawing.Size(463, 67);
            this.answer2.TabIndex = 2;
            this.answer2.Text = "button1";
            this.answer2.UseVisualStyleBackColor = false;
            this.answer2.Visible = false;
            this.answer2.Click += new System.EventHandler(this.answer2_Click);
            this.answer2.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.answer2_PreviewKeyDown);
            // 
            // answer3
            // 
            this.answer3.BackColor = System.Drawing.SystemColors.HighlightText;
            this.answer3.Location = new System.Drawing.Point(0, 432);
            this.answer3.Name = "answer3";
            this.answer3.Size = new System.Drawing.Size(463, 67);
            this.answer3.TabIndex = 3;
            this.answer3.Text = "button2";
            this.answer3.UseVisualStyleBackColor = false;
            this.answer3.Visible = false;
            this.answer3.Click += new System.EventHandler(this.answer3_Click);
            this.answer3.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.answer3_PreviewKeyDown);
            // 
            // answer4
            // 
            this.answer4.BackColor = System.Drawing.SystemColors.HighlightText;
            this.answer4.Location = new System.Drawing.Point(0, 514);
            this.answer4.Name = "answer4";
            this.answer4.Size = new System.Drawing.Size(463, 67);
            this.answer4.TabIndex = 4;
            this.answer4.Text = "button3";
            this.answer4.UseVisualStyleBackColor = false;
            this.answer4.Visible = false;
            this.answer4.Click += new System.EventHandler(this.answer4_Click);
            this.answer4.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.answer4_PreviewKeyDown);
            // 
            // answer5
            // 
            this.answer5.BackColor = System.Drawing.SystemColors.HighlightText;
            this.answer5.Location = new System.Drawing.Point(0, 597);
            this.answer5.Name = "answer5";
            this.answer5.Size = new System.Drawing.Size(463, 67);
            this.answer5.TabIndex = 5;
            this.answer5.Text = "button4";
            this.answer5.UseVisualStyleBackColor = false;
            this.answer5.Visible = false;
            this.answer5.Click += new System.EventHandler(this.answer5_Click);
            this.answer5.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.answer5_PreviewKeyDown);
            // 
            // answer6
            // 
            this.answer6.BackColor = System.Drawing.SystemColors.HighlightText;
            this.answer6.Location = new System.Drawing.Point(0, 675);
            this.answer6.Name = "answer6";
            this.answer6.Size = new System.Drawing.Size(463, 67);
            this.answer6.TabIndex = 6;
            this.answer6.Text = "button5";
            this.answer6.UseVisualStyleBackColor = false;
            this.answer6.Visible = false;
            this.answer6.Click += new System.EventHandler(this.answer6_Click);
            this.answer6.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.answer6_PreviewKeyDown);
            // 
            // questionDescribe
            // 
            this.questionDescribe.Location = new System.Drawing.Point(25, 132);
            this.questionDescribe.Name = "questionDescribe";
            this.questionDescribe.Size = new System.Drawing.Size(405, 128);
            this.questionDescribe.TabIndex = 7;
            this.questionDescribe.Text = "现在我们来模拟一个需求，比如现在有一个成绩单工作表，我们希望获得各科目不及格同学的名字。此时我们只需要在上面创建的工作簿项目中添加一个ComboBox，一个But" +
    "ton，一个textbox。在button的Click事件中添加下面的代码：";
            this.questionDescribe.Visible = false;
            // 
            // nextBtn
            // 
            this.nextBtn.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.nextBtn.Location = new System.Drawing.Point(0, 794);
            this.nextBtn.Name = "nextBtn";
            this.nextBtn.Size = new System.Drawing.Size(463, 67);
            this.nextBtn.TabIndex = 8;
            this.nextBtn.Text = "下一题";
            this.nextBtn.UseVisualStyleBackColor = false;
            this.nextBtn.Visible = false;
            this.nextBtn.Click += new System.EventHandler(this.nextBtn_Click);
            this.nextBtn.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.nextBtn_PreviewKeyDown);
            // 
            // AnswerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.nextBtn);
            this.Controls.Add(this.questionDescribe);
            this.Controls.Add(this.answer6);
            this.Controls.Add(this.answer5);
            this.Controls.Add(this.answer4);
            this.Controls.Add(this.answer3);
            this.Controls.Add(this.answer2);
            this.Controls.Add(this.answer1);
            this.Controls.Add(this.beginBtn);
            this.Name = "AnswerControl";
            this.Size = new System.Drawing.Size(463, 875);
            this.Load += new System.EventHandler(this.AnswerControl_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button beginBtn;
        private System.Windows.Forms.Button answer1;
        private System.Windows.Forms.Button answer2;
        private System.Windows.Forms.Button answer3;
        private System.Windows.Forms.Button answer4;
        private System.Windows.Forms.Button answer5;
        private System.Windows.Forms.Button answer6;
        private System.Windows.Forms.Label questionDescribe;
        private System.Windows.Forms.Button nextBtn;
    }
}
