﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RiskEvaluate
{
    enum AnalysisLogic
    {
        Sum = 0,
        Multi = 1,
        Maxer = 2,
        Logic6_1 = 3,
        Logic6_2 = 4,
        SumMultiAsPlus = 5,
        MultiMultiAsPlus = 6,
        Logic8_3 = 7,
        Logic1_4 = 8,
    }
    class Selection
    {
        public string description {get; set;}
        public int weight {get; set;}

    }

    class DetailQuestion
    {
        public string description {get; set;}
        public List<Selection> selections{get; set;}
        public List<int> selected {get; set;}
       public bool isMulti { get; set; }
    }

    class QuestionModel
    {
        /// <summary>
        /// 题号，小类为区分标准，类似2.3，不是2，也不是2.3.3
        /// </summary>
        public string serialNum { get; set; }

        public int analysisLogic { get; set; }
        public List<DetailQuestion> detailQuestions { get; set; }

        public int bandldId { get; set; }
    }
}
